'use strict';

const pkg = require('./package'),
  Code = require('code'),
  Hapi = require('hapi'),
  Lab = require('lab'),
  Inert = require('inert'),
  Vision = require('vision'),
  Basic = require('hapi-auth-basic'),
  Blipp = require('blipp'),
  HapiSwagger = require('hapi-swagger'),
  Promise = require('bluebird'),
  handlers = require('./lib/handlers.js'),
  Routes = require('./lib/routes.js');

const server = new Hapi.Server();

server.connection({ 
    port: 3000,
    host: '0.0.0.0',
    router: {
        stripTrailingSlash: true
    }
  });

module.exports = server;

server.register(Basic, function (err) {
    server.auth.strategy('simple', 'basic', { validateFunc: handlers.validate });
});

server.register.attributes = {
    pkg: pkg
};

server.register([
  Inert,
  Vision,
  Blipp,
  {
    register: HapiSwagger,
    options: {
        auth: 'simple'
    }
  }],
  function (err) {

    server.start((err) => {
        if (err) {throw err;}
            console.log('Server running at:', server.info.uri);
    });
  }
);

server.route(Routes);

//handlers.createUser('niktest', 'qwerty', 'default test user');