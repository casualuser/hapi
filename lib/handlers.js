'use strict';

const	Sequelize = require('sequelize'),
		mongoose = require('mongoose'),
		Bcrypt = require('bcrypt'),
		Boom = require('boom'),
		uuid = require('node-uuid');

var Product = require('../lib/model.js');
var dbURI = 'mongodb://localhost/hapi-lastid';

mongoose.connect(dbURI);
mongoose.connection.on('connected', function () {
    console.log('Mongoose default connection succesfully open to ' + dbURI);
});
mongoose.connection.on('error', function (err) {
    console.log(['error'], 'moungodb connect error: ' + err);
});

var sequelize = new Sequelize('mysql://root:@localhost:8306/LastId', { logging: false });

var User = sequelize.define('User', {
  id: { 
    type: Sequelize.STRING,
    primaryKey: true,
    autoIncrement: true,
    unique: true,
    allowNull: false
  },
  userid: Sequelize.STRING,
  username: {
      type: Sequelize.STRING,
      primaryKey: true
  },
  password: Sequelize.STRING,
  description: Sequelize.TEXT
});

function createUser(username, password, description) {

    if (typeof(description)==='undefined') description = '';
    var userid = uuid.v4();

    return User.create({
        userid:      userid,
        username:    username,
        password:    Bcrypt.hashSync(password, 10),
        description: description
    });
}

function getProducts(callback) {

    Product.find({}, function (error, data) {
        if (error) {
            return callback({
                statusCode: 503,
                message: 'Failed to get data',
                data: error
            })
        } else {
            return callback({
                statusCode: 200,
                message: 'Product Data Successfully Fetched',
                data: data
            })
        }
    });
}

function addProduct(name, description, price, barcode, category) {

    return Product.create({
            name: name,
            description: description,
            price: price,
            barcode: barcode,
            category: category
        });
}

function validate (request, username, password, callback) {

    Promise.resolve(
        User.findAll({
            where: {username: username},
            raw: true
        }))
        .then(function (result) {

            if (!result.length ) {
                return callback(Boom.unauthorized('Username not found, please check username spelling and try again or register new one'), false);
            }

            var _password = result[0].password;
            Bcrypt.compare(password, _password, (err, isValid) => {
                callback(null, true, {id: result[0].userid, name: result[0].username});
            });
        })
        .catch(function (error) {
            console.log(error);
        });
};

exports.createUser = createUser;
exports.getProducts = getProducts;
exports.addProduct = addProduct;
exports.validate = validate;