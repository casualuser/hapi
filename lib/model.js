'use strict';

var mongoose = require('mongoose');

var ProductSchema = new mongoose.Schema({
    name: String,
    description: String,
    price: Number,
    barcode: Number,
    category: String
});

var Product = mongoose.model('Product', ProductSchema);

module.exports = Product;