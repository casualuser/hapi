'use strict';

var handlers = require('../lib/handlers.js'),
    mongoose = require('mongoose'),
    Joi = require('joi'),
    Boom = require('boom');

module.exports = [

{   
    method: 'GET',
    path: '/',
    config: {
        handler: function (request, reply) {
          return reply('ok');
        }
    }
},
{   
    method: 'POST',
    path: '/register',
    config: {
        tags: ['api'],
        description: 'Register new user',
        notes: 'Register new user',
        validate: {
            payload: {
                username: Joi.string().required(),
                password: Joi.string().required(),
                description: Joi.string().default('user default description')
            }
        }
    },
    handler: function (request, reply) {

        var data = request.payload;
        handlers.createUser(data.username, data.password, data.description).then(reply('new user with username "' + data.username + '" have been successfuly registered'));
    }
},
{
    method: 'GET',
    path: '/login',
    config: {
        tags: ['api'],
        description: 'User login',
        notes: 'User login',
        auth: 'simple'
    },
    handler: function (request, reply) {
        return reply({
            statusCode: 200,
            message: 'Hello, you are successfully logged in! Wellcome home, '+ request.auth.credentials.name +' !',
            data: 'login data'
        });
    }
},
{
    method: 'GET',
    path: '/logout',
    config: {
        tags: ['api'],
        description: 'User logout',
        notes: 'User logout'
    },        
    handler: function (request, reply) {
        return reply(
            Boom.unauthorized('User logged out')
            );
//            return reply.redirect('http://log:out@localhost:3000').code(401);
    }
},
{
    method: 'GET',
    path: '/product',
    config: {
        tags: ['api'],
        description: 'Products list',
        notes: 'Products list',
        auth: 'simple'
    },
    handler: function (request, reply) {

        return handlers.getProducts(reply);
    }
},
{
    method: 'POST',
    path: '/product/add',
    config: {
        tags: ['api'],
        description: 'Add new product',
        notes: 'Add new product',
        auth: 'simple',
        validate: {
            payload: {
                name: Joi.string().required(),
                description: Joi.string().required(),
                price: Joi.number().integer().required(),
                barcode: Joi.number().integer().min(1000000000000).max(9999999999999).required(),
                category: Joi.string().required()
            }
        }
    },
    handler: function (request, reply) {

        var data = request.payload;

        handlers.addProduct(data.name, data.description, data.price, data.barcode, data.category)
            .then(reply('New product successfully added'));
    }
}
]